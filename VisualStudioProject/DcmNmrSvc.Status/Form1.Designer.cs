﻿namespace DcmNmrSvc.Status
{
    partial class DcmNmrSvcMon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DcmNmrSvcMon));
            this.DicomStatusLamp = new System.Windows.Forms.TextBox();
            this.Title = new System.Windows.Forms.Label();
            this.DicomLabel = new System.Windows.Forms.Label();
            this.NamerLabel = new System.Windows.Forms.Label();
            this.NamerStatusLamp = new System.Windows.Forms.TextBox();
            this.DicomStartBtn = new System.Windows.Forms.Button();
            this.NamerStartBtn = new System.Windows.Forms.Button();
            this.DicomStopBtn = new System.Windows.Forms.Button();
            this.NamerStopBtn = new System.Windows.Forms.Button();
            this.DicomRestartBtn = new System.Windows.Forms.Button();
            this.NamerRestartBtn = new System.Windows.Forms.Button();
            this.TrayIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.TrayIconContext = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.hideToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TrayIconContext.SuspendLayout();
            this.SuspendLayout();
            // 
            // DicomStatusLamp
            // 
            this.DicomStatusLamp.BackColor = System.Drawing.Color.Silver;
            this.DicomStatusLamp.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.DicomStatusLamp.Enabled = false;
            this.DicomStatusLamp.Location = new System.Drawing.Point(67, 43);
            this.DicomStatusLamp.MaxLength = 1;
            this.DicomStatusLamp.Name = "DicomStatusLamp";
            this.DicomStatusLamp.ReadOnly = true;
            this.DicomStatusLamp.Size = new System.Drawing.Size(20, 20);
            this.DicomStatusLamp.TabIndex = 0;
            // 
            // Title
            // 
            this.Title.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.Title.AutoSize = true;
            this.Title.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Title.Location = new System.Drawing.Point(42, 9);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(255, 20);
            this.Title.TabIndex = 1;
            this.Title.Text = "Dicom \\ Namer Service Monitor";
            this.Title.DoubleClick += new System.EventHandler(this.Title_Click);
            // 
            // DicomLabel
            // 
            this.DicomLabel.AutoSize = true;
            this.DicomLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DicomLabel.Location = new System.Drawing.Point(12, 44);
            this.DicomLabel.Name = "DicomLabel";
            this.DicomLabel.Size = new System.Drawing.Size(47, 16);
            this.DicomLabel.TabIndex = 2;
            this.DicomLabel.Text = "Dicom";
            // 
            // NamerLabel
            // 
            this.NamerLabel.AutoSize = true;
            this.NamerLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NamerLabel.Location = new System.Drawing.Point(12, 74);
            this.NamerLabel.Name = "NamerLabel";
            this.NamerLabel.Size = new System.Drawing.Size(49, 16);
            this.NamerLabel.TabIndex = 3;
            this.NamerLabel.Text = "Namer";
            // 
            // NamerStatusLamp
            // 
            this.NamerStatusLamp.BackColor = System.Drawing.Color.Silver;
            this.NamerStatusLamp.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.NamerStatusLamp.Enabled = false;
            this.NamerStatusLamp.Location = new System.Drawing.Point(67, 73);
            this.NamerStatusLamp.MaxLength = 1;
            this.NamerStatusLamp.Name = "NamerStatusLamp";
            this.NamerStatusLamp.ReadOnly = true;
            this.NamerStatusLamp.Size = new System.Drawing.Size(20, 20);
            this.NamerStatusLamp.TabIndex = 4;
            // 
            // DicomStartBtn
            // 
            this.DicomStartBtn.Location = new System.Drawing.Point(93, 41);
            this.DicomStartBtn.Name = "DicomStartBtn";
            this.DicomStartBtn.Size = new System.Drawing.Size(75, 23);
            this.DicomStartBtn.TabIndex = 5;
            this.DicomStartBtn.Text = "Start";
            this.DicomStartBtn.UseVisualStyleBackColor = true;
            this.DicomStartBtn.Click += new System.EventHandler(this.DicomStartBtn_Click);
            // 
            // NamerStartBtn
            // 
            this.NamerStartBtn.Location = new System.Drawing.Point(93, 71);
            this.NamerStartBtn.Name = "NamerStartBtn";
            this.NamerStartBtn.Size = new System.Drawing.Size(75, 23);
            this.NamerStartBtn.TabIndex = 5;
            this.NamerStartBtn.Text = "Start";
            this.NamerStartBtn.UseVisualStyleBackColor = true;
            this.NamerStartBtn.Click += new System.EventHandler(this.NamerStartBtn_Click);
            // 
            // DicomStopBtn
            // 
            this.DicomStopBtn.Location = new System.Drawing.Point(174, 41);
            this.DicomStopBtn.Name = "DicomStopBtn";
            this.DicomStopBtn.Size = new System.Drawing.Size(75, 23);
            this.DicomStopBtn.TabIndex = 5;
            this.DicomStopBtn.Text = "Stop";
            this.DicomStopBtn.UseVisualStyleBackColor = true;
            this.DicomStopBtn.Click += new System.EventHandler(this.DicomStopBtn_Click);
            // 
            // NamerStopBtn
            // 
            this.NamerStopBtn.Location = new System.Drawing.Point(174, 71);
            this.NamerStopBtn.Name = "NamerStopBtn";
            this.NamerStopBtn.Size = new System.Drawing.Size(75, 23);
            this.NamerStopBtn.TabIndex = 5;
            this.NamerStopBtn.Text = "Stop";
            this.NamerStopBtn.UseVisualStyleBackColor = true;
            this.NamerStopBtn.Click += new System.EventHandler(this.NamerStopBtn_Click);
            // 
            // DicomRestartBtn
            // 
            this.DicomRestartBtn.Location = new System.Drawing.Point(255, 41);
            this.DicomRestartBtn.Name = "DicomRestartBtn";
            this.DicomRestartBtn.Size = new System.Drawing.Size(75, 23);
            this.DicomRestartBtn.TabIndex = 5;
            this.DicomRestartBtn.Text = "Restart";
            this.DicomRestartBtn.UseVisualStyleBackColor = true;
            this.DicomRestartBtn.Click += new System.EventHandler(this.DicomRestartBtn_Click);
            // 
            // NamerRestartBtn
            // 
            this.NamerRestartBtn.Location = new System.Drawing.Point(255, 71);
            this.NamerRestartBtn.Name = "NamerRestartBtn";
            this.NamerRestartBtn.Size = new System.Drawing.Size(75, 23);
            this.NamerRestartBtn.TabIndex = 5;
            this.NamerRestartBtn.Text = "Restart";
            this.NamerRestartBtn.UseVisualStyleBackColor = true;
            this.NamerRestartBtn.Click += new System.EventHandler(this.NamerRestartBtn_Click);
            // 
            // TrayIcon
            // 
            this.TrayIcon.ContextMenuStrip = this.TrayIconContext;
            this.TrayIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("TrayIcon.Icon")));
            this.TrayIcon.Text = "Dicom \\ Namer Service Monitor";
            this.TrayIcon.Visible = true;
            this.TrayIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.TrayIcon_MouseDoubleClick);
            // 
            // TrayIconContext
            // 
            this.TrayIconContext.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.hideToolStripMenuItem,
            this.showToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.TrayIconContext.Name = "TrayIconContext";
            this.TrayIconContext.Size = new System.Drawing.Size(104, 70);
            // 
            // hideToolStripMenuItem
            // 
            this.hideToolStripMenuItem.Name = "hideToolStripMenuItem";
            this.hideToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.hideToolStripMenuItem.Text = "Hide";
            this.hideToolStripMenuItem.Click += new System.EventHandler(this.hideToolStripMenuItem_Click);
            // 
            // showToolStripMenuItem
            // 
            this.showToolStripMenuItem.Name = "showToolStripMenuItem";
            this.showToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.showToolStripMenuItem.Text = "Show";
            this.showToolStripMenuItem.Click += new System.EventHandler(this.showToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // DcmNmrSvcMon
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(344, 112);
            this.Controls.Add(this.NamerRestartBtn);
            this.Controls.Add(this.NamerStopBtn);
            this.Controls.Add(this.NamerStartBtn);
            this.Controls.Add(this.DicomRestartBtn);
            this.Controls.Add(this.DicomStopBtn);
            this.Controls.Add(this.DicomStartBtn);
            this.Controls.Add(this.NamerStatusLamp);
            this.Controls.Add(this.NamerLabel);
            this.Controls.Add(this.DicomLabel);
            this.Controls.Add(this.Title);
            this.Controls.Add(this.DicomStatusLamp);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DcmNmrSvcMon";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Service Monitor";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.TrayIconContext.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox DicomStatusLamp;
        private System.Windows.Forms.Label Title;
        private System.Windows.Forms.Label DicomLabel;
        private System.Windows.Forms.Label NamerLabel;
        private System.Windows.Forms.TextBox NamerStatusLamp;
        private System.Windows.Forms.Button DicomStartBtn;
        private System.Windows.Forms.Button NamerStartBtn;
        private System.Windows.Forms.Button DicomStopBtn;
        private System.Windows.Forms.Button NamerStopBtn;
        private System.Windows.Forms.Button DicomRestartBtn;
        private System.Windows.Forms.Button NamerRestartBtn;
        private System.Windows.Forms.NotifyIcon TrayIcon;
        private System.Windows.Forms.ContextMenuStrip TrayIconContext;
        private System.Windows.Forms.ToolStripMenuItem hideToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
    }
}

