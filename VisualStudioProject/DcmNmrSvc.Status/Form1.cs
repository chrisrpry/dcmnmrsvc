﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.ServiceProcess;
/* include system.serviceprocess and add the .dll as a reference*/
namespace DcmNmrSvc.Status
{
    public partial class DcmNmrSvcMon : Form
    {
        public DcmNmrSvcMon()
        {
            InitializeComponent();
        }
        //define vars for Dicom and Namer Services
        ServiceController DCMSvc = new ServiceController("DCMSvc");
        ServiceController NMRSvc = new ServiceController("NMRSvc");

        private void Form1_Load(object sender, EventArgs e)//On Form Load
        {
            //Check Service(s) Installed.
            if ((ServiceController.GetServices().Any(s => s.ServiceName == "DCMSvc") == false) || 
                (ServiceController.GetServices().Any(s => s.ServiceName == "NMRSvc") == false))
            {
                MessageBox.Show(this,"Service(s) not found, Application will now exit!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                this.Close();
            }
             //Set a timer to refresh the lamps.
                Timer timer = new Timer();
                timer.Interval = (1 * 500); // 0.5 Secs
                timer.Tick += new EventHandler(timer_Tick);
                timer.Start();
        }
        private void timer_Tick(object sender, EventArgs e)//items to refresh
        {
            StatusLamps();
        }
        private void StatusLamps()//Logic for Lamp Colour Status
        {
            //check if DCMSvc is running
            DCMSvc.Refresh();
            if (DCMSvc.Status.Equals(ServiceControllerStatus.Running))
            {
                //change lamp to green
                DicomStatusLamp.BackColor = Color.Green;
            }
            else if (DCMSvc.Status.Equals(ServiceControllerStatus.Stopped))
            {
                //change lamp to green
                DicomStatusLamp.BackColor = Color.Red;
            }
            else
            {
                //change lamp to Red
                DicomStatusLamp.BackColor = Color.Yellow;
            }
            //check if NMRSvc is running on launch
            NMRSvc.Refresh();
            if (NMRSvc.Status.Equals(ServiceControllerStatus.Running))
            {
                //change lamp to green
                NamerStatusLamp.BackColor = Color.Green;
            }
            else if (NMRSvc.Status.Equals(ServiceControllerStatus.Stopped))
            {
                //change lamp to green
                NamerStatusLamp.BackColor = Color.Red;
            }
            else
            {
                //change lamp to Red
                NamerStatusLamp.BackColor = Color.Yellow;
            }
        }
        //Service Buttons
        private void DicomStartBtn_Click(object sender, EventArgs e)//Dicom Start
        {
            if (DCMSvc.Status.Equals(ServiceControllerStatus.Stopped))
            {
                DCMSvc.Start();
            }
        }

        private void DicomStopBtn_Click(object sender, EventArgs e)//Dicom Stop
        {
            if (DCMSvc.Status.Equals(ServiceControllerStatus.Running))
            {
                DCMSvc.Stop();
            }
        }

        private void DicomRestartBtn_Click(object sender, EventArgs e)//Dicom Restart
        {
            if (DCMSvc.Status.Equals(ServiceControllerStatus.Running))
            {
                DCMSvc.Stop();
                DCMSvc.WaitForStatus(ServiceControllerStatus.Stopped);
                DCMSvc.Start();
            }
        }

        private void NamerStartBtn_Click(object sender, EventArgs e)//Namer Start
        {
            if (NMRSvc.Status.Equals(ServiceControllerStatus.Stopped))
            {
                NMRSvc.Start();
            }
        }

        private void NamerStopBtn_Click(object sender, EventArgs e)//Namer Stop
        {
            if (NMRSvc.Status.Equals(ServiceControllerStatus.Running))
            {
                NMRSvc.Stop();
            }
        }

        private void NamerRestartBtn_Click(object sender, EventArgs e)//Namer Restart
        {
            if (NMRSvc.Status.Equals(ServiceControllerStatus.Running))
            {
                NMRSvc.Stop();
                NMRSvc.WaitForStatus(ServiceControllerStatus.Stopped);
                NMRSvc.Start();
            }
        }

        private void Title_Click(object sender, EventArgs e)//Dev Info
        {
            MessageBox.Show(this,"Chris Perry - Elekta", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        //Context Menu Buttons and Tray Logic
        private void hideToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void showToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Show();
            this.Activate();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void TrayIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Show();
            this.Activate();
        }
    }
}
